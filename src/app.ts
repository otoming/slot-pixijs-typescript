import { Application, Sprite, loader } from "pixi.js";
import "./assets/css/main.css";
import { Result } from "@app/result.class";
import { WinHighlight } from "@app/winHighlight.class";
import { Balance } from "@app/balance.class";
import { SpinButton } from "@app/spinButton.class";
import { Debug } from "@app/debug.class";

const REEL_WIDTH = 160;
const REEL_HEIGHT = 260;
const SYMBOL_SIZE = 130;
const REEL_POSITION_X = 186;
const REEL_POSITION_Y = 50;
const DEBUG = true;

let result: Result;
let winHighlight: WinHighlight;
let balance: Balance;
let spinButton: SpinButton;
let debug: Debug;

enum STATE {
  INIT,
  SPIN,
  RESULT
}

let currentState = STATE.INIT;

class Game {

  private readonly app: Application;

  constructor() {
    // instantiate app
    this.app = new Application({
      width: 800,
      height: 600,
      backgroundColor: 0x000000
    });

	   winHighlight = new WinHighlight(this.app);
	   balance = new Balance();

    if (DEBUG) {
      debug = new Debug(balance);
    }

    // create view in DOM
    document.body.appendChild(this.app.view);

    // preload needed assets
    loader.add("STARFIELD", "/assets/img/starfield.png");
    loader.add("SYMBOLS", "/assets/sprites/symbols.json");
    loader.add("BUTTONS", "/assets/sprites/buttons.json");

    // then launch app
    loader.load(this.setup.bind(this));
  }

  private setup(): void {
    // append background
    const backgroundSprite = new Sprite(loader.resources["STARFIELD"].texture);
    this.app.stage.addChild(backgroundSprite);

    // Build the reels
    const reels: any = [];
    const reelContainer = new PIXI.Container();
    reelContainer.position.x = REEL_POSITION_X;
    reelContainer.position.y = REEL_POSITION_Y;

    const slotTextures: any = Object.keys(loader.resources.SYMBOLS.textures).map(
      (key) => loader.resources.SYMBOLS.textures[key]);

    // Add mask
    const mask = new PIXI.Graphics();
    mask.beginFill(0x000000, 0);
    mask.drawRect(REEL_POSITION_X, REEL_POSITION_Y + 50, REEL_WIDTH * 3, REEL_HEIGHT);
    this.app.stage.addChild(mask);
    reelContainer.mask = mask;

    this.app.stage.addChild(balance.getEl());

    for ( let i = 0; i < 3; i++) {
      const rc = new PIXI.Container();
      rc.x = i * REEL_WIDTH;
      reelContainer.addChild(rc);

      const reel: any = {
        container: rc,
        symbols: [],
        position: Math.floor(Math.random() * slotTextures.length),
        previousPosition: 0,
        blur: new PIXI.filters.BlurFilter()
      };
      reel.blur.blurX = 0;
      reel.blur.blurY = 0;
      rc.filters = [reel.blur];

      // Build the symbols
      for (let j = 0; j < slotTextures.length; j++) {
        const symbol = new PIXI.Sprite(slotTextures[j]);
        // Scale the symbol to fit symbol area.
        symbol.y = j * SYMBOL_SIZE;
        symbol.scale.x = symbol.scale.y = Math.min( SYMBOL_SIZE / symbol.width, SYMBOL_SIZE / symbol.height);
        symbol.x = Math.round((SYMBOL_SIZE - symbol.width) / 2);
        reel.symbols.push( symbol );
        rc.addChild(symbol);
      }
      reels.push(reel);
    }
    this.app.stage.addChild(reelContainer);

    // Function to start playing.
    function startPlay(): void {

      if (currentState === STATE.SPIN || balance.getBalance() <= 0) { return; }

      currentState = STATE.SPIN;
      spinButton.setTexture("BUTTONDOWN");
      balance.spin();
      winHighlight.removeWinGraphics();

      const reelsCombination: number[] = [];

      for (let i = 0; i < reels.length; i++) {
        const r = reels[i];
        const random = Math.floor(Math.random() * slotTextures.length);
        const extra = Math.random() > 0.5 ? 0.5 : 0;

        let target: number = r.position + 10 + i * 5 + extra + random;

        if (debug.getDebugMode()) {
          const debugCombination = debug.getDebugCombination();
          target = r.position - r.position % 5 + 10 + i * 5 + debugCombination[i];
        }
        reelsCombination.push(target % 5);
        tweenTo(r, "position", target, 2000 + i * 500, backout(0.6), null, i === reels.length - 1 ? reelsComplete.bind(reelsCombination) : null);
      }

    }

    // Reels done handler.
    function reelsComplete(): void {
      currentState = STATE.RESULT;
      const currentCombination = this;
      result = new Result(currentCombination);
      const winResult = result.checkCombination();
      winHighlight.setCombintaion(result.checkCombination());
      winHighlight.showWinLines();
      winHighlight.blinkPaytable();
      Object.keys(winResult).forEach((key) => {
        winResult[key] > 0 ? balance.win(winResult[key]) : "";
      });
      spinButton.setTexture("BUTTON");

    }

    // Listen for animate update.
    this.app.ticker.add(() => {
      // Update the slots.
      for ( let i = 0; i < reels.length; i++) {
        const r = reels[i];
        // Update blur filter y amount based on speed.
        // This would be better if calculated with time in mind also. Now blur depends on frame rate.
        r.blur.blurY = (r.position - r.previousPosition) * 8;
        r.previousPosition = r.position;

        // Update symbol positions on reel.
        for ( let j = 0; j < r.symbols.length; j++) {
          const s = r.symbols[j];
          s.y = (r.position + j) % r.symbols.length * SYMBOL_SIZE - SYMBOL_SIZE;
        }
      }
    });

    spinButton = new SpinButton(loader.resources.BUTTONS);

    const button = spinButton.getEl();

    button
        .on("pointerdown", onButtonDown)
        .on("pointerover", onButtonOver)
        .on("pointerout", onButtonOut);

    this.app.stage.addChild(button);

    function onButtonDown(): void {
      startPlay();
    }

    function onButtonOver(): void {
      if (currentState === STATE.SPIN) {
        return;
      }
      spinButton.setTexture("BUTTONOVER");
    }

    function onButtonOut(): void {
      if (currentState === STATE.SPIN) {
        return;
      }
      spinButton.setTexture("BUTTON");
    }

    const tweening: any = [];

    function tweenTo(object: any, property: string, target: number, time: number, easing: any, onchange: any, oncomplete: any): object {
      const tween = {
        object,
        property,
        propertyBeginValue: object[property],
        target,
        easing,
        time,
        change: onchange,
        complete: oncomplete,
        start: Date.now()
      };

      tweening.push(tween);
      return tween;
    }

    // Listen for animate update.
    this.app.ticker.add(() => {
      const now = Date.now();
      const remove = [];
      for (let i = 0; i < tweening.length; i++) {
        const t = tweening[i];
        const phase = Math.min(1, (now - t.start) / t.time);

        t.object[t.property] = lerp(t.propertyBeginValue, t.target, t.easing(phase));
        if (t.change) { t.change(t); }
        if (phase === 1) {
          t.object[t.property] = t.target;
          if (t.complete) {
            t.complete(t);
          }
          remove.push(t);
        }
      }
      for (let i = 0; i < remove.length; i++) {
        tweening.splice(tweening.indexOf(remove[i]), 1);
      }
    });

    // Basic lerp funtion.
    function lerp(a1: number, a2: number, t: number): number {
      return a1 * (1 - t) + a2 * t;
    }

    function backout(amount: any): any {
      return (t: number) => {
        return (--t * t * ((amount + 1) * t + amount) + 1);
      };
    }

  }
}

new Game();
