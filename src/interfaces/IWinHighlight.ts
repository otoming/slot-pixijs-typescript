export interface IWinHighlight {
  setCombintaion(result: any): any;
  showWinLines(): void;
  blinkPaytable(): void;
  removeWinGraphics(): void;
}
