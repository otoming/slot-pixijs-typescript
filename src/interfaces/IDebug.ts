export interface IDebug {
  getDebugMode(): boolean;
  getDebugCombination(): number[];
}
