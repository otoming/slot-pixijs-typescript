export interface ISpinButton {
  getEl(): PIXI.Sprite;
  setTexture(button: string): void;
}
