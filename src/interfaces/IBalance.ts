export interface IBalance {
  getEl(): PIXI.Text;
  getBalance(): number;
  spin(): void;
  win(amount: number): void;
  setDebugValue(amount: number): void;
}
