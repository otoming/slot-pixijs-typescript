/*
codeTable = {
			0: {"center": "2BAR"},
			0.5: {"top": "BAR", "bottom": "2BAR"},
			1: {"center": "BAR"},
			1.5: {"top": "3BAR", "bottom": "BAR"},
			2: {"center": "3BAR"},
			2.5: {"top": "CHERRY", "bottom": "3BAR"},
			3: {"center": "CHERRY"},
			3.5: {"top": "SEVEN", "bottom": "CHERRY"},
			4: {"center": "SEVEN"},
			4.5: {"top": "2BAR", "bottom": "SEVEN"},
		}
 */

import {IResult} from "@interfaces/IResult";

export class Result implements IResult{
	private combination: number[];
	private result: any = {top: 0, center: 0, bottom: 0};

	constructor(combination: number[]) {
		this.combination = combination;
	}

	public checkCombination() {
		const equal = this.combination.every((val: any) => val === this.combination[0]);

		if (equal) {
			switch (this.combination[0]) {
				case 0: {
					this.result.center = 20;
					break;
				}
				case 0.5: {
					this.result.top = 10;
					this.result.bottom = 20;
					break;
				}
				case 1: {
					this.result.center = 5;
					break;
				}
				case 1.5: {
					this.result.top = 50;
					this.result.bottom = 5;
					break;
				}
				case 2: {
					this.result.center = 50;
					break;
				}
				case 2.5: {
					this.result.top = 2000;
					this.result.bottom = 50;
					break;
				}
				case 3: {
					this.result.center = 1000;
					break;
				}
				case 3.5: {
					this.result.top = 150;
					this.result.bottom = 4000;
					break;
				}
				case 4: {
					this.result.center = 150;
					break;
				}
				case 4.5: {
					this.result.top = 20;
					this.result.bottom = 150;
					break;
				}
				default: {
					break;
				}
			}
		}
		if (this.arrayContainsArray(this.combination, [2.5, 3.5])) {
			this.result.top = 75;
		}
		if (this.arrayContainsArray(this.combination, [3, 4])) {
			this.result.center = 75;
		}
		if (this.arrayContainsArray(this.combination, [3.5, 4.5])) {
			this.result.bottom = 75;
		}
		if (this.arrayContainsArray([0, 1, 2], this.combination)) {
			this.result.center = 5;
		}
		if (this.arrayContainsArray([0.5, 1.5, 2.5], this.combination)) {
			this.result.bottom = 5;
		}
		if (this.arrayContainsArray([0.5, 1.5, 4.5], this.combination)) {
			this.result.top = 5;
		}

		return this.result;
	}

	private arrayContainsArray(superset: any, subset: any) {
		if (0 === subset.length) {
			return false;
		}
		return subset.every((value: any) => {
			return (superset.indexOf(value) >= 0);
		});
	}

}
