import {Application, Graphics} from "pixi.js";
import {IWinHighlight} from "@interfaces/IWinHighlight";

export class WinHighlight implements IWinHighlight{
	private app: Application;
	private result: any;
	private graphics: Graphics[] = [];
	private interval: any;
	private delta: number = 0.1;

	constructor(app: Application) {
		this.app = app;
	}

	public setCombintaion(result: any): void {
		this.result = result;
	}

	public showWinLines(): void {
		if (this.result.top > 0) {
			this.drawLine(200, 150);
		}
		if (this.result.center > 0) {
			this.drawLine(200, 225);
		}
		if (this.result.bottom > 0) {
			this.drawLine(200, 300);
		}
	}

	public blinkPaytable(): void {
		if (this.result.top === 2000) {
			this.blinkRect(180, 440);
		} else if (this.result.center === 1000) {
			this.blinkRect(180, 500);
		} else if (this.result.bottom === 4000) {
			this.blinkRect(180, 560);
		}

		for (const key in this.result) {
			if (this.result.hasOwnProperty(key)) {
				if (this.result[key] === 150) {
					this.blinkRect(460, 440);
				}
				if (this.result[key] === 75) {
					this.blinkRect(460, 500);
				}
				if (this.result[key] === 5) {
					this.blinkRect(460, 560);
				}
				if (this.result[key] === 50) {
					this.blinkRect(740, 440);
				}
				if (this.result[key] === 20) {
					this.blinkRect(740, 500);
				}
				if (this.result[key] === 10) {
					this.blinkRect(740, 560);
				}
			}
		}
	}

	public removeWinGraphics(): void {
		this.graphics.forEach((graphics) => {
			graphics.destroy();
		});
		this.graphics = [];
		clearInterval(this.interval);
	}

	private drawLine(x: number, y: number): void {
		const graphics = new Graphics();
		graphics.beginFill(0xFF0000);
		graphics.drawRect(x, y, 425, 10);
		this.graphics.push(graphics);
		this.app.stage.addChild(graphics);
	}

	private blinkRect(x: number, y: number): void {
		const graphics = new Graphics();

		graphics.beginFill(0x0000FF);
		graphics.drawRect(x, y, 60, 40);
		this.graphics.push(graphics);
		this.app.stage.addChild(graphics);
		let delta = this.delta;

		this.interval = setInterval(blink, 10);

		function blink(): void {
			graphics.alpha += delta;
			if (graphics.alpha >= 0.5) {
				delta = -0.01;
			} else if (graphics.alpha <= 0.1) {
				delta = +0.01;
			}
		}

	}

}
