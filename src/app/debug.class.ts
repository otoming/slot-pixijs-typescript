import {Balance} from "@app/balance.class";
import {IDebug} from "@interfaces/IDebug";

export class Debug implements IDebug{

	private balance: Balance;
	private debugMode: boolean = false;
	private debugCombination: number[] = [];

	constructor(balance: Balance) {
		this.balance = balance;
		this.init();
	}

	public getDebugMode(){
		return this.debugMode;
	}
	public getDebugCombination(){
		this.debugCombination = [];
		for ( let i = 1; i<= 3; i++) {
			let symbol = (<HTMLSelectElement>document.getElementById("symbol" + i));
			let symbolValue = symbol.options[symbol.selectedIndex].value;
			let position = (<HTMLSelectElement>document.getElementById("position" + i));
			let positionValue = position.options[position.selectedIndex].value;
			this.debugCombination.push(+symbolValue + +positionValue);
		}
		return this.debugCombination;
	}

	private init() {
		const acc = document.getElementsByClassName("accordion");
		acc[0].addEventListener("click", function() {
			this.classList.toggle("active");
			const panel = this.nextElementSibling;
			if (panel.style.maxHeight) {
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});

		const optionSymbolLists = document.getElementsByName('symbol');

		for (const sub of optionSymbolLists as any) {
			let optionList = (<HTMLSelectElement>document.getElementById(sub.id)).options;

			let options = [
				{
					text: '3xBAR',
					value: '2'
				},
				{
					text: 'BAR',
					value: '1'
				},
				{
					text: '2xBAR',
					value: '0'
				},
				{
					text: '7',
					value: '4'
				},
				{
					text: 'CHERRY',
					value: '3'
				}
			];

			options.forEach(option =>
				optionList.add(
					new Option(option.text, option.value)
				)
			);

		}

		const optionPositionLists = document.getElementsByName('position');

		for (const sub of optionPositionLists as any) {
			let optionList = (<HTMLSelectElement>document.getElementById(sub.id)).options;

			let options = [
				{
					text: 'Top',
					value: '-0.5'
				},
				{
					text: 'Center',
					value: '0'
				},
				{
					text: 'Bottom',
					value: '0.5'
				}
			];

			options.forEach(option =>
				optionList.add(
					new Option(option.text, option.value)
				)
			);

		}

		(<any>window).switchDebugMode =  (button: HTMLButtonElement) => {
			if (button.value === "off"){
				this.debugMode = true;
				button.value = "on";
				button.innerText = "Debug mode ON";
			}
			else{
				this.debugMode = false;
				button.value = "off";
				button.innerText = "Debug mode OFF";
			}
		};

		(<any>window).updateBalance =  (input: any) => {
			if (input.value > 5000) {
				input.value = 5000;
			}
			else if (input.value < 1) {
				input.value = 1;
			}

			this.balance.setDebugValue(input.value);
		};

	}

}
