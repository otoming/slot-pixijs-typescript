import {ISpinButton} from "@interfaces/ISpinButton";

export class SpinButton implements ISpinButton{
  private button: PIXI.Sprite;
  private resource: PIXI.loaders.Resource;

  constructor(resource: PIXI.loaders.Resource) {
    this.resource = resource;
    this.button = new PIXI.Sprite(this.resource.textures["BUTTON"]);
    this.button.interactive = true;
    this.button.buttonMode = true;
    this.button.cursor = "hover";
    this.button.x = 675;
    this.button.y = 205;
  }

  public getEl() {
    return this.button;
  }

  public setTexture(button: string) {
    this.button.texture = this.resource.textures[button];
  }

}
