import { IBalance } from "@interfaces/IBalance";

export class Balance implements IBalance{
	private balance: number = 1337;
	private balanceText: PIXI.Text;

	constructor() {
		this.balanceText = new PIXI.Text("Balance: " + this.balance, {
			fontFamily: "Arial",
			fontSize: 32,
			fontWeight: "bold", fill: ["#ffffff"]});
		this.balanceText.x = 40;
		this.balanceText.y = 40;
	}

	public getEl () {
		return this.balanceText;
	}

	public getBalance () {
		return this.balance;
	}

	public spin() {
		if (--this.balance <= 0){
			this.balanceText.text = "Insert coin to continue";
		}
		else {
			this.setBalance(this.balance);
		}
	}

	public win(amount: number) {
		this.setBalance(this.balance += amount);
}

	public setDebugValue(amount: number){
		this.balance = amount;
		this.setBalance(this.balance);
	}

	private setBalance(balance: number) {
		this.balanceText.text = "Balance: " + balance;
	}
}
