PixiJS + Typescript + Webpack

Prerequisite:
* install node
* run `npm i` before first start

Run (dev): `npm start` then browse http://localhost:4200/ with Chrome
Build (prod): `npm run build` then use freshly generated "dist"
